<?php

namespace TheFeed\Service;

use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\Exception\ServiceException;

class UtilisateurService implements UtilisateurServiceInterface
{
    public function __construct(private UtilisateurRepository $utilisateurRepository)
     {}
    public function creerUtilisateur($login, $motDePasse, $email, $donneesPhotoDeProfil): void
    {
        //TO-DO
        if (
            isset($login) && isset($motDePasse) && isset($email)
            && isset($donneesPhotoDeProfil)
        ) {

            if (strlen($login) < 4 || strlen($login) > 20) {
                throw new ServiceException("Login petit");
            }
            if (!preg_match("#^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,20}$#", $motDePasse)) {
                throw new ServiceException("mdp non conforme");
            }
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                throw new ServiceException("adresse mail bizarre");
            }
            $utilisateurRepository = $this->utilisateurRepository;
            $utilisateur = $utilisateurRepository->recupererParLogin($login);
            if ($utilisateur != null) {
                throw new ServiceException("Ce login est déjà pris!");
            }

            $utilisateur = $utilisateurRepository->recupererParEmail($email);
            if ($utilisateur != null) {
                throw new ServiceException("Un compte est déjà enregistré avec cette adresse mail!");
            }
            $explosion = explode('.', $donneesPhotoDeProfil['name']);
            $fileExtension = end($explosion);
            if (!in_array($fileExtension, ['png', 'jpg', 'jpeg'])) {
                throw new ServiceException("La photo de profil n'est pas au bon format!");
            }
            $pictureName = uniqid() . '.' . $fileExtension;
            $from = $donneesPhotoDeProfil['tmp_name'];
            $to = __DIR__ . "/../../ressources/img/utilisateurs/$pictureName";
            move_uploaded_file($from, $to);
            $mdpHache = MotDePasse::hacher($motDePasse);
            $utilisateur = Utilisateur::create($login, $mdpHache, $email, $pictureName);
            $utilisateurRepository->ajouter($utilisateur);

        } else {
            throw new ServiceException("Login, nom, prenom ou mot de passe manquant.");
        }
    }

    public function connecterUtilisateur($login, $mdp): void
    {
        if ((isset($login) && isset($mdp))) {
            $utilisateurRepository = $this->utilisateurRepository;
            /** @var Utilisateur $utilisateur */
            $utilisateur = $utilisateurRepository->recupererParLogin($_POST["login"]);

            if ($utilisateur == null) {
                throw new ServiceException("Login inconnu");

            }
            if (!MotDePasse::verifier($mdp, $utilisateur->getMdpHache())) {
                throw new ServiceException("MDP incorrect");
            }

            ConnexionUtilisateur::connecter($utilisateur->getIdUtilisateur());
        } else {
            throw new ServiceException("login et mdp manquants");
        }

    }

    public function deconnecterUtilisateur() : void{
        if (ConnexionUtilisateur::estConnecte()) {
        ConnexionUtilisateur::deconnecter();
        }else{
            throw new ServiceException("Utilisateur non connecté.");
        }
    }

    public function recupererUtilisateurParId($idUtilisateur, $autoriserNull = true): ?Utilisateur
    {
        $utilisateur = ($this->utilisateurRepository)->recupererParClePrimaire($idUtilisateur) ?? null;
        if (!$autoriserNull && !$utilisateur) {
            throw new ServiceException("idUtilisateur null, autoriserNull false");
        }
        return $utilisateur;
    }
}