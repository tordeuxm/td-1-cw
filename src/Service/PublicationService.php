<?php

namespace TheFeed\Service;

use TheFeed\Controleur\ControleurPublication;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\Exception\ServiceException;

class PublicationService implements PublicationServiceInterface
{
    public function __construct(private PublicationRepository $publicationRepository, private UtilisateurRepository $utilisateurRepository)
    {
    }

    public function recupererPublications()
    {
        $publications = ($this->publicationRepository)->recuperer();
        return $publications;
    }

    public function creerPublication($idUtilisateur, $message): void
    {
        $utilisateur = ($this->utilisateurRepository)->recupererParClePrimaire($idUtilisateur);

        if ($utilisateur == null) {
            throw new ServiceException("Mon message d'erreur!");
        }

        if ($message == null || $message == "") {
            throw new ServiceException("Mon message d'erreur!");
        }
        if (strlen($message) > 250) {
            throw new ServiceException("Mon message d'erreur!");
        }

        $publication = Publication::create($message, $utilisateur);
        ($this->publicationRepository)->ajouter($publication);
    }

    public function recupererPublicationsUtilisateur($idUtilisateur)
    {
        return ($this->publicationRepository)->recupererParAuteur($idUtilisateur);
    }
}
