<?php

namespace TheFeed\Service;

interface PublicationServiceInterface
{
    public function recupererPublications();

    public function creerPublication($idUtilisateur, $message): void;

    public function recupererPublicationsUtilisateur($idUtilisateur);
}