<?php

namespace TheFeed\Controleur;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Routing\Generator\UrlGenerator;
use TheFeed\Lib\Conteneur;
use TheFeed\Lib\MessageFlash;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;


class ControleurGenerique
{

    protected function afficherVue(string $cheminVue, array $parametres = []): Response
    {
        extract($parametres);
        $messagesFlash = MessageFlash::lireTousMessages();
        ob_start();
        require __DIR__ . "/../vue/$cheminVue";
        $corpsReponse = ob_get_clean();
        return new Response($corpsReponse);
    }

    // https://stackoverflow.com/questions/768431/how-do-i-make-a-redirect-in-php
    protected function rediriger(string $nomRoute, array $query = []): RedirectResponse
    {
        /** @var UrlGenerator $generateurUrl */
        $generateurUrl = Conteneur::recupererService("generateurUrl");
        /** @var UrlHelper $assistantUrl */
        $assistantUrl = Conteneur::recupererService("assistantUrl");
        $url = $assistantUrl->getAbsoluteUrl($generateurUrl->generate($nomRoute, $query));
        return new RedirectResponse($url);
    }

    public function afficherErreur($messageErreur = "a", $statusCode = 400): Response
    {
        $reponse = self::afficherTwig('erreur.html.twig', ["messageErreur" => $messageErreur]);
        $reponse->setStatusCode($statusCode);
        return $reponse;
    }

    protected function afficherTwig(string $cheminVue, array $parametres = []): Response
    {
        /** @var Environment $twig */
        $twig = Conteneur::recupererService("twig");
        $corpsReponse = $twig->render($cheminVue, $parametres);
        return new Response($corpsReponse);
    }


}