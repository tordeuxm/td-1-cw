<?php

namespace TheFeed\Controleur;

use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Configuration\Configuration;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Lib\MotDePasse;
use TheFeed\Modele\DataObject\Utilisateur;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use Symfony\Component\HttpFoundation\Response;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
use TheFeed\Service\UtilisateurService;

class ControleurUtilisateur extends ControleurGenerique
{

    public function  __construct(private PublicationService $publicationService, private UtilisateurService $utilisateurService){}






    public function afficherErreur($messageErreur = "", $statusCode = 400): Response
    {
        return parent::afficherErreur($messageErreur, $statusCode);
    }

    #[Route(path: "/utilisateurs/{idUtilisateur}/publications", name: 'afficherPublication', methods:["GET"])]
    public function afficherPublications($idUtilisateur): Response
    {
        $utilisateur = ($this->utilisateurService)->recupererUtilisateurParId($idUtilisateur);
        if($utilisateur) {
            $loginHTML = htmlspecialchars($utilisateur->getLogin());
            return $this->afficherTwig("publication/page_perso.html.twig",
                ['publications' => ($this->publicationService)->recupererPublicationsUtilisateur($idUtilisateur), "log" => $loginHTML]);
        }else{
            throw new ServiceException("erreur utilisateur vide");
        }
//com
    }
    #[Route(path: '/inscription', name: 'afficherFormulaireInscription', methods:["GET"])]
    public function afficherFormulaireCreation(): Response
    {
        return $this->afficherTwig("utilisateur/inscription.html.twig");
    }
    #[Route(path: '/inscription', name: 'inscription', methods:["POST"])]
    public function creerDepuisFormulaire(): \Symfony\Component\HttpFoundation\Response
    {
        $login = $_POST['login'] ?? null;
        $motDePasse = $_POST['mot-de-passe'] ?? null;
        $adresseMail = $_POST['email'] ?? null;
        $nomPhotoDeProfil = $_FILES['nom-photo-de-profil'] ?? null;
        try {
            ($this->utilisateurService)->creerUtilisateur($login, $motDePasse, $adresseMail, $nomPhotoDeProfil);
        } catch (ServiceException $e) {
            MessageFlash::ajouter('error' ,$e->getMessage());
            return $this->afficherTwig("utilisateur/inscription.html.twig");
        }
        MessageFlash::ajouter("success", "L'utilisateur a bien été créé !");
        return $this->rediriger('afficherListe');


    }
    #[Route(path: '/connexion', name: 'afficherFormulaireConnexion', methods:["GET"])]
    public function afficherFormulaireConnexion(): Response
    {
        /*$reponse = $this->afficherTwig("base.html.twig", [
            "page_title" => "Formulaire de connexion",
            "cheminVueBody" => "utilisateur/connexion.html.twig",
            "method" => Configuration::getDebug() ? "get" : "post",
        ]);
        $reponse->send();*/
        return $this->afficherTwig("utilisateur/connexion.html.twig");


    }

    #[Route(path: '/connexion', name: 'connecter', methods:["POST"])]
    public function connecter(): Response
    {
        $login = $_POST['login'] ?? null;
        $mdp = $_POST['mot-de-passe'];
        try {
            ($this->utilisateurService)->connecterUtilisateur($login, $mdp);
        }catch (ServiceException $e){
            MessageFlash::ajouter('error', $e->getMessage());
            return $this->afficherTwig("utilisateur/connexion.html.twig");
        }
        MessageFlash::ajouter("success", "Vous êtes connectés");
         return $this->rediriger("afficherListe");
    }
    #[Route(path: '/deconnexion', name: 'deconnecter', methods:["GET"])]
    public function deconnecter(): Response
    {
        try {
            ($this->utilisateurService)->deconnecterUtilisateur();
        }catch (ServiceException $e){
            MessageFlash::ajouter('error', $e->getMessage());
            return $this->rediriger("afficherListe");
        }
        return $this->rediriger("afficherListe");
    }
}
