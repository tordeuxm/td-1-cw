<?php

namespace TheFeed\Controleur;

use http\Env\Response;
use Symfony\Component\Routing\Attribute\Route;
use TheFeed\Lib\ConnexionUtilisateur;
use TheFeed\Lib\MessageFlash;
use TheFeed\Modele\DataObject\Publication;
use TheFeed\Modele\Repository\PublicationRepository;
use TheFeed\Modele\Repository\UtilisateurRepository;
use TheFeed\Service\Exception\ServiceException;
use TheFeed\Service\PublicationService;
class ControleurPublication extends ControleurGenerique
{

    public function  __construct(private PublicationService $publicationService){}

    #[Route(path: '/', name:'afficherListeListeConnecte', methods:["GET"])]
    #[Route(path: '/publications', name:'afficherListe', methods:["GET"])]
    public function afficherListe() : \Symfony\Component\HttpFoundation\Response
    {
        $service = $this->publicationService;
         return $this->afficherTwig("publication/feed.html.twig", ['publications' => $service->recupererPublications()]);
    }


    #[Route(path: '/publications', name: 'creerPublication', methods: ["POST"])]
    public function creerDepuisFormulaire() : \Symfony\Component\HttpFoundation\Response
    {
     $idUtilisateurConnecte = ConnexionUtilisateur::getIdUtilisateurConnecte();
     $message = $_POST['message'];
     try {
         ($this->publicationService)->creerPublication($idUtilisateurConnecte, $message);
     }
     catch(ServiceException $e) {
         MessageFlash::ajouter('error' ,$e->getMessage());
         return $this->rediriger('afficherListe');
     }

     return $this->rediriger('afficherListe');
 }


}
